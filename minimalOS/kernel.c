/*
 * Desc: Minimal OS, to show how to boot an x86 machine. UFCFWK-15-2
 */
#if !defined(__cplusplus)
#include <stdbool.h> /* C doesn't have booleans by default. */
#endif
#include <stddef.h>
#include <stdint.h>
 
/* Check if the compiler thinks we are targeting the wrong operating system. */
#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif
 
/* This tutorial will only work for the 32-bit ix86 targets. */
#if !defined(__i386__)
#error "This tutorial needs to be compiled with a ix86-elf compiler"
#endif
 
/* Hardware text mode color constants. */
enum vga_color {
	COLOR_BLACK = 0,
	COLOR_BLUE = 1,
	COLOR_GREEN = 2,
	COLOR_CYAN = 3,
	COLOR_RED = 4,
	COLOR_MAGENTA = 5,
	COLOR_BROWN = 6,
	COLOR_LIGHT_GREY = 7,
	COLOR_DARK_GREY = 8,
	COLOR_LIGHT_BLUE = 9,
	COLOR_LIGHT_GREEN = 10,
	COLOR_LIGHT_CYAN = 11,
	COLOR_LIGHT_RED = 12,
	COLOR_LIGHT_MAGENTA = 13,
	COLOR_LIGHT_BROWN = 14,
	COLOR_WHITE = 15,
};
 
uint8_t make_color(enum vga_color fg, enum vga_color bg) {
  return fg | bg << 4;
}
 
uint16_t make_vgaentry(char c, uint8_t color) {
  uint16_t c16 = c;
  uint16_t color16 = color;
  return c16 | color16 << 8;
}
 
size_t strlen(const char* str) {
  size_t ret = 0;
  while ( str[ret] != 0 )
    ret++;
  return ret;
}

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;
 
void terminal_initialize() {
  terminal_row = 0;
  terminal_column = 0;
  terminal_color = make_color(COLOR_LIGHT_GREY, COLOR_BLACK);
  terminal_buffer = (uint16_t*) 0xB8000;
  for (size_t y = 0; y < VGA_HEIGHT; y++) {
    for (size_t x = 0; x < VGA_WIDTH; x++) {
      const size_t index = y * VGA_WIDTH + x;
      terminal_buffer[index] = make_vgaentry(' ', terminal_color);
    }
  }
}
 
void terminal_setcolor(uint8_t color) {
  terminal_color = color;
}
 
void terminal_putentryat(char c, uint8_t color, size_t x, size_t y) {
  const size_t index = y * VGA_WIDTH + x;
  terminal_buffer[index] = make_vgaentry(c, color);
}

/**************************************** CHANGE 01 ****************************************/

void next_line(uint8_t color)
{
	terminal_row++; //add one row to the console
	terminal_column = 0 
}

void scroll_down(){
	
	size_t buffer; //stores the characters temporarily

	///moving everything to the top 
	for (buffer = 0; buffer < (VGA_HEIGHT * VGA_WIDTH) - VGA_WIDTH; buffer++)
	{
		terminal_buffer[buffer] = terminal_buffer[buffer + VGA_WIDTH];
	}

	//move one line to the top
	for (buffer = ((VGA_HEIGHT - 1) * VGA_WIDTH); buffer < (VGA_HEIGHT * VGA_WIDTH); buffer++)
	{
		terminal_buffer[buffer] = make_vgaentry(NULL, terminal_color);
	}

	//move up before going to the nextLine
	terminal_row--;
}

/************************************ END OF CHANGE 01 ************************************/



void terminal_putchar(char c) 
{

/**************************************** CHANGE 02 ****************************************/

	//check whether the newline character is called
	if (c == '\n')
	{
		//move to next line
		next_line(terminal_color);

		if (terminal_row+1 > VGA_HEIGHT)
		{
			//scroll down
			scroll_down();
		}
		
	}
		
	else
	{
		terminal_putentryat(c, terminal_color, terminal_column, terminal_row);

	  	if (++terminal_column == VGA_WIDTH) 
		{
			//move to next line
    	        	next_line(terminal_color);

		    	if (++terminal_row == VGA_HEIGHT) 
			{
				//scroll down
		      		scroll_down();
		    	}
	  	}	
	}

/************************************ END OF CHANGE 02 ************************************/

}
 
void terminal_writestring(const char* data) {
  size_t datalen = strlen(data);
  for (size_t i = 0; i < datalen; i++)
    terminal_putchar(data[i]);
}

#if defined(__cplusplus)
extern "C" /* Use C linkage for kernel_main. */
#endif
void kernel_main() {
  /* Initialize terminal interface */
  terminal_initialize();
  
/**************************************** CHANGE 03 ****************************************/

	/* Testing the implementation of newline and scrolling */

	//print message 6 times in red
	for(int i = 0; i < 6; i++) 
	{
		terminal_color = COLOR_RED;
		terminal_writestring("MinimalOS done by Alson!\n");
	}

	//print message 18 times in blue
	for(int i = 0; i < 18; i++)
	{
		terminal_color = COLOR_BLUE;
		terminal_writestring("MinimalOS done by Alson!\n");
	}

	//print message 5 times in green
	for(int i = 0; i < 5; i++) 
	{
		terminal_color = COLOR_GREEN;
		terminal_writestring("MinimalOS done by Alson!\n");
	}

/************************************ END OF CHANGE 03 ************************************/

}

